"use strict"

const gameField = [
    'x', 'o', 'o',
    'o', null, 'o',
    'x', 'o', 'o'
    ];

if ( gameField[0] == 'x' && gameField[1] == 'x' && gameField[2] == 'x' ||
     gameField[3] == 'x' && gameField[4] == 'x' && gameField[5] == 'x' ||
     gameField[6] == 'x' && gameField[7] == 'x' && gameField[8] == 'x' ||
     gameField[0] == 'x' && gameField[3] == 'x' && gameField[6] == 'x' ||
     gameField[1] == 'x' && gameField[4] == 'x' && gameField[7] == 'x' ||
     gameField[2] == 'x' && gameField[5] == 'x' && gameField[8] == 'x' ||
     gameField[0] == 'x' && gameField[4] == 'x' && gameField[8] == 'x' ||
     gameField[6] == 'x' && gameField[4] == 'x' && gameField[2] == 'x' ) {
	alert ("Winner - Cross"); }
else if ( gameField[0] == 'o' && gameField[1] == 'o' && gameField[2] == 'o' ||
     gameField[3] == 'o' && gameField[4] == 'o' && gameField[5] == 'o' ||
     gameField[6] == 'o' && gameField[7] == 'o' && gameField[8] == 'o' ||
     gameField[0] == 'o' && gameField[3] == 'o' && gameField[6] == 'o' ||
     gameField[1] == 'o' && gameField[4] == 'o' && gameField[7] == 'o' ||
     gameField[2] == 'o' && gameField[5] == 'o' && gameField[8] == 'o' ||
     gameField[0] == 'o' && gameField[4] == 'o' && gameField[8] == 'o' ||
     gameField[6] == 'o' && gameField[4] == 'o' && gameField[2] == 'o' ) {
	alert ("Winner - Circle"); }
  else { alert("Draw") }
